<?php include('session.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>class detail</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/sty">
</head>
<body>
	<?php include('navbar.php'); ?>
	<div class="container">
		<?php 
			include('connection.php');
			class Detailofclass{
				private $connection;
				public function __construct(){
					$connection= new Connection();
					$this->conn=$connection->connect();
				}
				
				public function select_class_data(){
					if (isset($_POST['search-box'])){
						$search_info=$_POST['search-box'];
						$searching="SELECT * FROM class WHERE class='$search_info'" or
						die("could not search");
						$query=mysqli_query($this->conn,$searching);
						$count=mysqli_num_rows($query);
						if ($count == 0) {
							echo "no data found";
						}
						else{
							echo "<table class='table table-striped table-bordered classdetail-table'>
								<tr class='classdetail-heading'>
								<th>Classes</th>
								<th colspan='2'>
									<form action='' method='POST'>
									<div class='input-group search'>
										      <input type='text' name='search-box' class='form-control' placeholder='Search for...'>
										      <span class='input-group-btn'>
										        	<button class='btn btn-default' type='submit'><i class='fa fa-search' aria-hidden='true'></i></button>
										      </span>
										   </div>
									</form>
								</th>
								</tr>
								<tr class='classdetail-heading'>
									<th>CLASSID</th>
									<th>CLASS</th>
									<th>Actions</th>
								</tr>";
								while($row = mysqli_fetch_array($query))
								{
									echo "<tr>";
										echo "<td>" . $row['classid'] . "</td>";
										echo "<td><a href='eachclass.php?id=".$row['classid']."'>" . $row['class'] . "</a></td>";
										echo "<td><a href='updateclass.php?id=".$row['classid']."'><i class='fa fa-pencil-square'></i></a><a href='dropdown.php?id=".$row['classid']."'><i class='fa fa-plus-square'></i></a></td>";
										// echo "<td><a href='dropdown.php?id=".$row['classid']."'><i class='fa fa-plus-square'></i></a></td>";
									echo "</tr>";
								}
							echo "</table>";
						}
					}
					else{
						if (!isset($_GET['page']) || $_GET['page'] =="" || $_GET['page'] =="1"){
							$page1=0;
						}
						else{
							$page = $_GET['page'];
							$page1 = ($page * 5)-5;
						}
						$sql="SELECT * FROM class ORDER BY classid DESC LIMIT $page1,6";
						$query=mysqli_query($this->conn,$sql);
						if ($query) {
							echo "<table class='table table-striped table-bordered classdetail-table'>
								<tr class='classdetail-heading'>
								<th>Classes</th>
								<th colspan='2'>
									<form action='' method='POST'>
									<div class='input-group search'>
										      <input type='text' name='search-box' class='form-control' placeholder='Search for...'>
										      <span class='input-group-btn'>
										        	<button class='btn btn-default' type='submit'><i class='fa fa-search' aria-hidden='true'></i></button>
										      </span>
										   </div>
									</form>
								</th>
								</tr>
								<tr class='classdetail-heading'>
									<th>CLASSID</th>
									<th>CLASS</th>
									<th>Actions</th>
								</tr>";

								while($row = mysqli_fetch_array($query))
								{
									echo "<tr>";
										echo "<td>" . $row['classid'] . "</td>";
										echo "<td><a href='eachclass.php?id=".$row['classid']."'>" . $row['class'] . "</a></td>";
										echo "<td><a href='updateclass.php?id=".$row['classid']."'><i class='fa fa-pencil-square'></i></a><a href='dropdown.php?id=".$row['classid']."'><i class='fa fa-plus-square'></i></a></td>";
											// echo "<td><a href='dropdown.php?id=".$row['classid']."'><i class='fa fa-plus-square'></i></a></td>";
									echo "</tr>";
								}
							echo "</table>";
							$sql1 = "SELECT * FROM class";
							$query1 = mysqli_query($this->conn,$sql1);
							$count = mysqli_num_rows($query1);
							$a = $count/5;
							$a = ceil($a);
							?>
							<ul class="pagination paging">
								<?php
								for ($i=1; $i<=$a; $i++) { 
									if ($i==1){
										?>
										<li class="page-item">
											<a class='page-link' href="classdetail.php?page=<?php echo $i;?>"><?php echo $i;?></a>
										</li>
										<?php
									}
									else{
										?>
										<li class="page-item">
												<a class='page-link' href="classdetail.php?page=<?php echo $i;?>"><?php echo $i;?></a>
											</li>
											<?php
									}
								}	?>

								<?php
								
								?>
							</ul>
							<?php
							
						}	
					}
				}
			}
			$class=new Detailofclass();
			$class->select_class_data();
		?>
		</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>