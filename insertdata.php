<?php 
	include("connection.php");
	class Insertdata{
		private $conn;
		public function __construct(){

			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function insert_teacher_data($name,$password,$username){
			$sql="INSERT INTO teacher (name,password,username) VALUES ('$name','$password','$username')";
			$query=mysqli_query($this->conn,$sql);
			return $query;
		}


		public function insert_student_data($student_roll,$student_name,$student_address,$student_class,$image){
			$sql="INSERT INTO students (RollNo, name, Address, class,imagename) VALUES ($student_roll, '$student_name', '$student_address', '$student_class','$image')";
			$query=mysqli_query($this->conn,$sql);
			return $query;

		}

		public function user_login($teacher_password,$teacher_username){
			$sql="SELECT * FROM teacher WHERE password='$teacher_password' AND username='$teacher_username'";
			$query=mysqli_query($this->conn, $sql);
			return $query;
		}

		public function update_teacher_info($id, $teacher_name,$teacher_password,$teacher_username){
			$sql="UPDATE teacher SET name='$teacher_name',password='$teacher_password',username='$teacher_username' WHERE id='$id'";
			$query=mysqli_query($this->conn,$sql);
			if($query){
				$res="SELECT * FROM teacher WHERE id='$id'";
				$result=mysqli_query($this->conn,$res);
				return $result;
			}
		}

		public function update_student_info($student_roll,$student_name,$student_address,$student_class,$image, $id){
			$sql="UPDATE students SET RollNo='$student_roll',name='$student_name',Address='$student_address',class='$student_class',imagename='$image' WHERE id='$id'";
			$query=mysqli_query($this->conn,$sql);
			return $query;
		}

		public function select_student_data(){
			$sql="SELECT * FROM students GROUP BY RollNo";
			$result=mysqli_query($this->conn,$sql);
			return $result;
		}
		public function select_student_image($id){
			$sql="SELECT * FROM students WHERE id='$id'";
			$result=mysqli_query($this->conn,$sql);
			return $result;
		}
		public function select_image($id){
			$sql="SELECT imagename FROM students WHERE id='$id' ";
			$query=mysqli_query($this->conn,$sql);
			return $query;
		}
		public function dropdown_data(){
			$sql="SELECT class FROM students GROUP BY class";
			$query=mysqli_query($this->conn,$sql);
			return $query;
		}
	}
?>