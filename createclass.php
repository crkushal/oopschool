<?php 
	include('connection.php');
	include 'session.php';
	class Createclass{
		public function __construct(){
			$connection= new Connection();
			$this->conn=$connection->connect();
		}
		public function create(){
			$valid=true;
			if ($_SERVER['REQUEST_METHOD']=="POST") {
				$class=$_POST['class'];
				if (empty($class)) {
					$valid=false;
					// echo "empty class data";
				}
				else{
					$checking="SELECT * FROM class WHERE class='$class'";
					$result=mysqli_query($this->conn,$checking);
					if (mysqli_num_rows($result) > 0) {
						$valid=false;
						$_SESSION['checking_error']="cannot have same class";
					}
					else{
						$valid=true;
						$sql="INSERT INTO class (class) VALUES ('$class')";
						$query=mysqli_query($this->conn,$sql);
						if ($query) {
							header("location:classdetail.php");
						}
						else{
							header("location:createclass.php");
						}
					}
				}
			}
		}
	}
	$createclass= new Createclass();
	$createclass->create();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Students Detail</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style type="text/css">
		.create-class{
			margin-top: 5%;
			background:#c1c7c4;
		}
		form{
			margin: 40px;
		}
		form label{
			font-size: 18px;
			font-weight: normal;
		    font-family: monospace;
		    padding-bottom: 2px;
		}
		form input{
			margin-bottom: 17px;
		}
	</style>
</head>
<body>
	<?php include('navbar.php'); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 create-class">
				<div class="register-heading">
					<label>Create Class</label>
				</div>
				<form method="POST" action="" onsubmit="creatingClass()">
					<div class="form-group">
					  <label>Class</label>
					  <input type="text" class="form-control" id="exampleInputEmail1" name="class" id="create-class">
					  <button type="submit" class="btn btn-primary" name="submit">submit</button>
					</div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>















