<?php 
	include("session.php");
	$register_studentname_error=$register_studentroll_error=$register_studentaddress_error=$register_studentclass_error=$checking_error="";
		$target="";
		$uploadok=1;
	if ($_SERVER['REQUEST_METHOD']=='POST' && $_POST['submit']){
		$valid=true;
		if (empty($_POST['studentname'])) {
			$valid=false;
			$register_studentname_error="name is missing";
		}
		if (empty($_POST['studentroll'])) {
			$valid=false;
			$register_studentroll_error="Rollno is missing";
		}
		if (empty($_POST['studentaddress'])) {
			$valid=false;
			$register_studentaddress_error="address is missing";
		}
		if (empty($_POST['class'])) {
			$valid=false;
			$register_studentclass_error="class is missing";
		}
		if ($valid) {
			include('insertdata.php');
			$student_name=$_POST['studentname'];
			$student_roll=$_POST['studentroll'];
			$student_address=$_POST['studentaddress'];
			$student_class=$_POST['class'];

			//for similar roll no in same class
			$checking="SELECT * FROM students WHERE class='$student_class' && RollNo='$student_roll'";
			if ($checking) {
				$valid=false;
				$_SESSION['checking_error']="cannot have same rollno";
			}


			$target="images/".basename($_FILES['image']['name']);
			$image=$_FILES['image']['name'];
			$imagefiletype=strtolower(pathinfo($target,PATHINFO_EXTENSION));
			if (file_exists($target)) {
				$uploadok=0;
				echo "this files already exist in the system";
			}
			else if($imagefiletype!="jpg" && $imagefiletype!="png" && $imagefiletype!="gif"){
				$uploadok=0;
				echo "this type of files arenot supported in the system";
			}
			else{
				$sql= new Insertdata();
				$result=$sql->insert_student_data($student_roll,$student_name,$student_address,$student_class,$image);
				if ($result && $uploadok) {
					if (move_uploaded_file($_FILES['image']['tmp_name'],$target)) {
						header('location:showdetail.php');
					}
					else{
						echo "cannot move the file to distionation";
					}
				}
				else{
					header('location:home.php');
				}
			}
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Students Detail</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
	<?php include('navbar.php'); ?>
	<?php 
		$student_name_value=isset($_POST['studentname']) || !empty($_POST['studentname'])?$_POST['studentname']:"";
		$student_roll_value=isset($_POST['studentroll']) || !empty($_POST['studentroll'])?$_POST['studentroll']:"";
		$student_address_value=isset($_POST['studentaddress']) || !empty($_POST['studentaddress'])?$_POST['studentaddress']:"";
		$student_class_value=isset($_POST['studentclass']) || !empty($_POST['studentclass'])?$_POST['studentclass']:"";
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content">
				<form method="POST" action="" enctype="multipart/form-data">
					<p class="register-heading">Add Student</p>
					<div class="form-group">
						<p>
							<?php if (isset($_SESSION['checking_error']) || !empty($_SESSION['checking_error'])) {
								echo $_SESSION['checking_error'];
							} ?>
						</p>
						<label>Student Name:</label>
						<input type="text" name="studentname" class="form-control" value="<?php echo $student_name_value?>">
						<p><?php echo $register_studentname_error; ?></p>
						<label>Address:</label>
						<input type="text" name="studentaddress" class="form-control" value="<?php echo $student_address_value?>">
						<p><?php echo $register_studentaddress_error; ?></p>
						<label>Roll No:</label>
						<input type="text" name="studentroll" class="form-control" value="<?php echo $student_roll_value?>">
						<p><?php echo $register_studentroll_error; ?></p>
						<label>Class:</label>
						<?php 
							require("insertdata.php");
							$dropdown= new Insertdata();
							$result=$dropdown->dropdown_data();
							echo "<select name='class' class='form-control'>";
							while ($row = mysqli_fetch_array($result)) {
		    					echo "<option class='form-control' value='" . $row['class'] ."'>" . $row['class'] ."</option>";
							}
							echo "</select>";
						?>
						<p><?php echo $register_studentclass_error; ?></p>
						<label>upload image:</label>
						<input type="file" name="image" class="form-control" id="uploadimageicon"><br>
						<input type="submit" name="submit" class="form-control" id="button">
						
					</div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>















