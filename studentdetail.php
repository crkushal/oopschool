
<!DOCTYPE html>
<html>
<head>
	<title>student detail</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
</head>
<body>
	<?php
	include("insertdata.php");
	include('session.php');
	class Studentdetail{
		public function show_student_detail(){
			$select= new insertdata();
			$result = $select->select_student_data();

			if(!$result){
				echo 'result not found';
			}

			else{
				if(isset($_SESSION['deleted']) || !empty($_SESSION['deleted'])){
					echo $_SESSION['deleted'];
			  		unset($_SESSION['deleted']);
				}
				echo "<div class='studentdetail-heading'>Studentdetail</div>";
				echo "<table class='table table-bordered table-striped'>
				<tr>
					<th>id</th>
					<th>RollNo</th>
					<th>name</th>
					<th>Address</th>
					<th>class</th>
					<th>Update</th>
					<th>Delete</>
				</tr>";

				while($row = mysqli_fetch_array($result))
				{
					echo "<tr>";
					echo "<td><a href='detailofeach.php?id=".$row['id']."'>" . $row['id'] . "</a></td>";
					echo "<td>" . $row['RollNo'] . "</td>";
					echo "<td>" . $row['name'] . "</td>";
					echo "<td>" . $row['Address'] . "</td>";
					echo "<td>" . $row['class'] . "</td>";
					echo "<td><a href='updatestudentform.php?id=".$row['id']."'>Update</a></td>";
					echo "<td><a href='deletedata.php?id=".$row['id']."'>Delete</a></td>";
					echo "</tr>";
				}
				echo "</table>";
			}
		}
	}
?>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>