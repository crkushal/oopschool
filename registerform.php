<?php 
	include('newsession.php');
	$register_name_error=$register_password_error=$register_username_error="";
	if ($_SERVER['REQUEST_METHOD']=='POST'){
		$valid=true;
		// if (empty($_POST['name'])){
		// 	$valid=false;
		// 	$register_name_error="name is missing";
		// }
		// if (empty($_POST['password'])) {
		// 	$valid=false;
		// 	$register_password_error="password is missing";
		// }
		// if (empty($_POST['username'])) {
		// 	$valid=false;
		// 	$register_username_error="username is missing";
		// }
		if ($valid) {
			require 'insertdata.php';
			$register_name=$_POST['name'];
			$pass=$_POST['password'];
			$register_password=md5($pass);
			$register_username=$_POST['username'];
			$sql= new insertdata();
			$result=$sql->insert_teacher_data($register_name,$register_password,$register_username);

			if ($result){
				header('location:loginform.php');
			}
			else{
				header('registerform.php');
			}
		}
	}
?>
 <!DOCTYPE html>
<html>
<head>
	<title>Registration Form</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<?php include('navbefore.php'); ?>
	<?php 
		$name_value=isset($_POST['name']) || !empty($_POST['name'])?$_POST['name']:"";
		$password_value=isset($_POST['password']) || !empty($_POST['password'])?$_POST['password']:"";
		$username_value=isset($_POST['username']) || !empty($_POST['username'])?$_POST['username']:"";
	 ?>
	 <div class="container-fluid " >
	 	<div class="row">
	 		<div class="col-md-4"></div>
	 		<div class="col-md-4 register-content">
	 			<form method="POST" action="" onsubmit="register()">
	 				<p class="register-heading">Registrarion</p>
				  	<div class="form-group ">
						<label>NAME:</label>
						<input type="text" class="form-control" name="name" id="register-name" value="<?php echo($name_value);?>"><p id="name-register"></p>
						<?php
							if(!empty($register_name_error)){
						?>
								<p class="alert-warning"><?php echo $register_name_error; ?></p>
						<?php } ?>
						<label>PASSWORD:</label>
						<input type="password" class="form-control" name="password" id="register-password" value="<?php echo($password_value);?>"><p id="password-register"></p>
						<?php
							if(!empty($register_password_error)){
						?>
								<p class="alert-warning"><?php echo $register_password_error; ?></p>
						<?php } ?>
						<label>USERNAME:</label>
						<input type="text" class="form-control" name="username" id="register-username" value="<?php echo($username_value);?>"><p id="username-register"></p>
						<?php
							if(!empty($register_username_error)){
						?>
								<p class="alert-warning"><?php echo $register_username_error; ?></p>
						<?php } ?>
						<button class="btn btn-primary" type="submit">submit</button>
					</div>
				</form>
	 		</div>
	 		<div class="col-md-4"></div>
	 	</div>
	 </div>
	<script type="text/javascript" src="custom.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>