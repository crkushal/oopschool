<?php 
	include('newsession.php');
	$login_password_error=$login_username_error="";
	if ($_SERVER['REQUEST_METHOD']=='POST'){
		$valid=true;
		if (empty($_POST['password'])) {
			$valid=false;
			$login_password_error="password is missing";
		}
		if (empty($_POST['username'])) {
			$valid=false;
			$login_username_error="username is missing";
		}
		if ($valid) {
			require('insertdata.php');
			$login_password=md5($_POST['password']);
			$login_username=$_POST['username'];

			$sql=new Insertdata();
			$result=$sql->user_login($login_password,$login_username);
			$row=mysqli_num_rows($result);
			

			if ($row > 0) {
				session_start();
				$detail=mysqli_fetch_array($result);
				$_SESSION['user']=$detail;
				header('location:classdetail.php');
			}
			else{
				$login_error = 'Invalid credentials';
				header('loginform.php');
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Form</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body >
	<?php include('navbefore.php'); ?>
	<?php
		$password_value_login=isset($_POST['password']) || !empty($_POST['password'])?$_POST['password']:"";
		$username_value_login=isset($_POST['username']) || !empty($_POST['username'])?$_POST['username']:"";
	?>
	 <div class="container">
	 	<div class="row">
	 		<div class="col-md-4"></div>
	 		<div class="col-md-4 register-content">
	 			<form method="POST" action="" onsubmit="loginVerification()">
	 				<p class="register-heading">Login Form</p>
				  	<div class="form-group ">
						<label>User Name:</label>
						<input type="text" name="username" id="login-username" class="form-control" value="<?php echo($username_value_login);?>">
						<!-- <?php
							if(!empty($login_username_error)){
						?>
								<p class="alert-warning"><?php echo $login_username_error; ?></p>
						<?php } ?> -->
						<label>Password:</label>
						<input type="password" name="password" id="login-password" class="form-control" value="<?php echo($password_value_login);?>">
						<!-- <?php
							if(!empty($login_password_error)){
						?>
								<p class="alert-warning"><?php echo $login_password_error; ?></p>
						<?php } ?> -->
						<button class="btn btn-primary" type="submit">submit</button>
					</div>
				</form>
	 		</div>
	 		<div class="col-md-4"></div>
	 	</div>
	</div>
	<script type="text/javascript" src="custom.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>