<?php 
	include('session.php');
	$update_name_error=$update_password_error=$update_username_error="";
	if ($_SERVER['REQUEST_METHOD']=='POST'){
		$valid=true;
		if (empty($_POST['name'])){
			$valid=false;
			$update_name_error="name is missing";
		}
		if (empty($_POST['username'])) {
			$valid=false;
			$update_username_error="username is missing";
		}
		if ($valid) {
			require 'insertdata.php';
			$id=$_POST['id'];
			$update_name=$_POST['name'];
			$pass=!empty($_POST['password']) ? md5($_POST['password']) : $_SESSION['user']['password'];
			$update_password=$pass;
			$update_username=$_POST['username'];
			$sql= new Insertdata();
			$result=$sql->update_teacher_info($id, $update_name,$update_password,$update_username);

			if ($result){
				$row=mysqli_num_rows($result);
				if ($row > 0) {
					$updated=mysqli_fetch_assoc($result);
					$_SESSION['user']=$updated;
					header('location:showdetail.php');
				}
			}
			else{
				header('location:updateteacherform.php');
			}
		}
	}
?>
 <!DOCTYPE html>
<html>
<head>
	<title>Update Teacher</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
	<?php include('navbar.php'); ?>
	<?php 
		$update_name_value=isset($_POST['name']) || !empty($_POST['name'])?$_POST['name']:$_SESSION['user']['name'];
		$update_password_value=isset($_POST['password']) || !empty($_POST['password'])?$_POST['password']:"";
		$update_username_value=isset($_POST['username']) || !empty($_POST['username'])?$_POST['username']:$_SESSION['user']['username'];
	 ?>
	 <div class="container">
	 	<div class="row">
	 		<div class="col-md-4"></div>
	 		<div class="col-md-4 register-content">
	 			<form method="POST" action="">
	 				<p class="register-heading">Update Profile</p>
	 				<div class="form-group">
						<input type="hidden" name="id" class="form-control" value="<?php echo $_SESSION['user']['id']; ?>">
						<label>NAME:</label>
						<input type="text" name="name" class="form-control" value="<?php echo($update_name_value);?>">
						<p><?php echo $update_name_error; ?></p>
						<label>PASSWORD:</label>
						<input type="password" name="password" class="form-control" value="<?php echo($update_password_value);?>">
						<p><?php echo $update_password_error; ?></p>
						<label>USERNAME:</label>
						<input type="text" name="username" class="form-control" value="<?php echo($update_username_value);?>">
						<p><?php echo $update_username_error; ?></p>
						<input type="submit" name="Register" id="button" class="btn btn-primary">
	 				</div>
	 			</form>
	 		</div>
	 		<div class="col-md-4"></div>
	 	</div>
	 </div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>