<?php 
	include('session.php');
	include('connection.php');
	class Update{
		private $connection;
		public function __construct(){
			$connection= new Connection();
			$this->conn=$connection->connect();
		}
		public function update_student($id,$classid){
			$valid=true;
			if ($_SERVER['REQUEST_METHOD']=="POST") {
				if (empty($_POST['studentname'])) {
					$valid=false;
					$error_name="user name is empty";
				}
				if (empty($_POST['studentroll'])) {
					$valid=false;
					$error_roll="user roll is empty";
				}
				if (empty($_POST['studentaddress'])) {
					$valid=false;
					$error_address="user address is empty";
				}
				if ($valid) {
					echo $id;
					$updatename=$_POST['studentname'];
					$updateroll=$_POST['studentroll'];
					$updateaddress=$_POST['studentaddress'];

					$sql="UPDATE student SET student_roll='$updateroll',student_name='$updatename',student_address='$updateaddress' WHERE student_id='$id' ";
					$query=mysqli_query($this->conn,$sql);
					print_r($query);
					if ($query) {
						header("location:eachclass.php?id=".$classid."");
					}
					else{
						echo "error in deleting the data";
					}
				}
			}

		}
	}
	$conn= new connection();
		$cn=$conn->connect();
		$id=$_GET['id'];
		if(empty($id)){
			header('location:update.php');
		}
		$sql=mysqli_query($cn, "SELECT * FROM student WHERE student_id='$id'") or die();
		$row = '';
		if(mysqli_num_rows($sql) > 0){
			$row = mysqli_fetch_assoc($sql);
		}else{
			header('location:showdetail.php');
		}
	$classid=$row['class_id'];
	$update = new Update();
	$update->update_student($id,$classid);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Students Detail</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style type="text/css">
		.heading{
		    font-size: 18px;
		    background:#4db6ac;
		    text-align: center;
		    font-family: monospace;
		    padding: 10px 0px 10px 0px;
		    margin: 0 -15px 0 -15px;
		}
		.middle{
			margin-top: 5%;
			background:#c1c7c4;
		}
		form{
			margin: 40px;
		}
		form label{
			font-size: 18px;
			font-weight: normal;
		    font-family: monospace;
		    padding-bottom: 2px;
		}
		form input{
			margin-bottom: 17px;
		}
	</style>
</head>
<body>
	<?php include('navbar.php'); ?>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 middle">
				<div class="register-heading">
					<label>UPDATE STUDENT</label>
				</div>
				<form method="POST" action="">
					<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"><br>
					<label>RollNo:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentroll" value="<?php echo $row['student_roll']; ?>">
					 <label>Name:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentname" value="<?php echo $row['student_name']; ?>">
					 <label>Address:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentaddress" value="<?php echo $row['student_address']; ?>">
					<button type="submit" class="btn btn-primary" name="submit">submit</button>
					</div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>