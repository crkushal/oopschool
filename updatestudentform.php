<?php 
	include('session.php');
	$update_studentname_error=$update_studentroll_error=$update_studentaddress_error=$update_studentclass_error="";
	if ($_SERVER['REQUEST_METHOD']=='POST' && $_POST['submit']){
		$valid=true;
		if (empty($_POST['studentname'])) {
			$valid=false;
			$update_studentname_error="name is missing";
		}
		if (empty($_POST['studentroll'])) {
			$valid=false;
			$update_studentroll_error="Rollno is missing";
		}
		if (empty($_POST['studentaddress'])) {
			$valid=false;
			$update_studentaddress_error="address is missing";
		}
		if (empty($_POST['studentclass'])) {
			$valid=false;
			$update_studentclass_error="class is missing";
		}
		if ($valid) {
			include('insertdata.php');
			$update_student_name=$_POST['studentname'];
			$update_student_roll=$_POST['studentroll'];
			$update_student_address=$_POST['studentaddress'];
			$update_student_class=$_POST['studentclass'];
			$id = $_POST['id'];
			$sql= new Insertdata();
			if ($_FILES['updateimage']['error'] == 4) {
				$valid=false;
				$update=$sql->select_image($id);
				$image = '';
				if(mysqli_num_rows($update) > 0){
					$image = mysqli_fetch_assoc($update)['imagename'];
				
					$result=$sql->update_student_info($update_student_roll,$update_student_name,$update_student_address,$update_student_class,$image, $id);
				}
				if ($result) {
					header('location:showdetail.php');
				}
				else{
					header('location:updatestudentform.php');
				}
			}
			else{
				$target="images/".basename($_FILES['updateimage']['name']);
				$imageupdate=$_FILES['updateimage']['name'];
				$imagefiletype=strtolower(pathinfo($target,PATHINFO_EXTENSION));
				if (file_exists($target)) {
					$uploadok=0;
					echo "this files already exist in the system";
				}
				else if($imagefiletype!="jpg" && $imagefiletype!="png" && $imagefiletype!="gif"){
					$uploadok=0;
					echo "this type of files arenot supported in the system";
				}
				else{
					$result=$sql->update_student_info($update_student_roll,$update_student_name,$update_student_address,$update_student_class,$imageupdate, $id);
					if ($result && $uploadok) {
						if (move_uploaded_file($_FILES['imagename']['tmp_name'],$target)) {
							header('location:showdetail.php');
						}
					}else{
						echo "cannot move the file to distionation";
					}
				}
				if ($result) {
					header('location:showdetail.php');
				}
				else{
					header('location:updatestudentform.php');
				}
			}
		}
	}
 ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Update Students</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	</head>
	<body>	
		<?php require"navbar.php"; ?>
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4 register-content">
					<form method="POST" action="" enctype="multipart/form-data">
						<p class="register-heading">Update Students</p>
						<?php 
							include('connection.php');
							$conn= new Connection();
							$cn=$conn->connect();
							$id=$_GET['id'];
							if(empty($id)){
								header('location:showdetail.php');
							}
							$sql=mysqli_query($cn,"SELECT * FROM students WHERE id='$id' ");
							$row = '';
							if(mysqli_num_rows($sql) > 0){
								$row = mysqli_fetch_assoc($sql);
							}else{
								header('location:showdetail.php');
							}
							?>
						<div class="form-group">
							<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
							<label>Student Name:</label>
							<input type="text" name="studentname" class="form-control" value="<?php echo $row['name'];?>">
							<p><?php echo $update_studentname_error; ?></p>
							<label>Address:</label>
							<input type="text" name="studentaddress" class="form-control" value="<?php echo $row['Address'];?>">
							<p><?php echo $update_studentaddress_error; ?></p>
							<label>Roll No:</label>
							<input type="text" name="studentroll" class="form-control" value="<?php echo $row['RollNo'];?>">
							<p><?php echo $update_studentroll_error; ?></p>
							<label>Class:</label>
							<input type="text" name="studentclass" class="form-control" value="<?php echo $row['class'];?>">
							<p><?php echo $update_studentclass_error; ?></p>
							<label>Upload Image:</label>
							<input type="file" name="updateimage" class="form-control" id="uploadimageicon" value="<?php echo $row['imagename']?>"><br>
							<button class="btn btn-primary" type="submit" name="submit">Submit</button>
						</div>
					</form>

				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>