<?php 
include('showeachdetail.php');
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Show Each Student Detail</title>
	<style type="text/css">
		h2{
			font-weight: bold;
			font-family: serif;
			font-style: inherit;
			text-align: center;
			font-size: 30px;
		}
		.container-fluid{
			text-align: center;
			font-size: 25px;
    		font-family: monospace;
    		font-weight: bolder;
    		
		}
	</style>
</head>
<body>
	<?php include('navbar.php'); ?>
	<h2>
		Student Detail:
	</h2>
		<div class="container-fluid">
			<?php 
			$id=$_GET['id'];
			$eachdetail = new Eachstudentdetail();
			$eachdetail->show_eachstudent_detail($id);
			?>
		</div>
</body>
</html>
