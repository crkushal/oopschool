 <?php include('session.php'); ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>eachclassdetail</title>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style type="text/css">
		.eachClass-heading{
			background-color: red;
		}
	</style>
 </head>
 <body>
 	<?php 
 	include('navbar.php');
 	?>
 	<div class="container">
 		<?php 
			include('connection.php');
			$id=$_GET['id'];
			class Eachclass{
				private $connection;
				public function __construct(){
					$connection= new Connection();
					$this->conn=$connection->connect();
				}
				public function select_class_info($id){
					if (isset($_POST['search-box-student'])) {
						$search = $_POST['search-box-student'];
						$sql="SELECT * FROM student WHERE (student_name LIKE '%$search%' AND class_id=$id) OR (student_roll='$search' AND class_id=$id)"or die("data doesn't exist");
						$query = mysqli_query($this->conn,$sql);
						$count = mysqli_num_rows($query);
						if ($count == 0) {
							echo "no data found";
						}
						else{
							$sql2="SELECT * FROM class WHERE classid='$id'";
							$query2=mysqli_query($this->conn,$sql2);
							$row2=mysqli_fetch_array($query2);
							echo "<table class='table table-striped table-bordered eachclass-table'>
							<tr class='eachclass-heading'>
										<th colspan='2'>
											<div class='eachclass-heading'>Student Of Class:".$row2['class']."</div>
										</th>
										<th  colspan='3'>
											<form action='' method='POST'>
												<div class='input-group search'>
													    <input type='text' name='search-box-student' class='form-control' placeholder='Search for...'>
													    <span class='input-group-btn'>
													        <button class='btn btn-default' type='submit'><i class='fa fa-search' aria-hidden='true'></i></button>
													    </span>
												</div>
											</form>
										</th>
									</tr>
								<tr>
									<th>ID</th>
									<th>ROLLNO</th>
									<th>NAME</th>
									<th>ADDRESS</th>
									<th>ACTIONS</th>
								</tr>";
								if (mysqli_num_rows($query)>0) {
									while($row1 = mysqli_fetch_array($query))
									{
										echo "<tr>";
											echo "<td>" . $row1['student_id'] . "</td>";
											echo "<td>" . $row1['student_roll'] . "</td>";
											echo "<td>" . $row1['student_name'] . "</td>";
											echo "<td>" . $row1['student_address'] . "</td>";
											echo "<td><a href='delete.php?id=".$row1['student_id']."'><i class='fa fa-trash'></i></a>
											<a href='update.php?id=".$row1['student_id']."'><i class='fa fa-pencil'></i></a></td>";
										echo "</tr>";
									}
								}
								else{
									$error="no data available";
								}
							echo "</table>";
						}
					}
					else{
						$error="";
						$sql="SELECT * FROM class WHERE classid='$id'";
						$query=mysqli_query($this->conn,$sql);
						if ($query) {
							$row=mysqli_fetch_array($query);
							$sql1="SELECT * FROM student WHERE class_id=$id";
							$result=mysqli_query($this->conn,$sql1);
							if ($query) {
								echo "<table class='table table-striped table-bordered eachclass-table'>
									<tr class='eachclass-heading'>
										<th colspan='2'>
											<div class='eachclass-heading'>Student Of Class:".$row['class']."</div>
										</th>
										<th  colspan='3'>
											<form action='' method='POST'>
												<div class='input-group search'>
													    <input type='text' name='search-box-student' class='form-control' placeholder='Search for...'>
													    <span class='input-group-btn'>
													        <button class='btn btn-default' type='submit'><i class='fa fa-search' aria-hidden='true'></i></button>
													    </span>
												</div>
											</form>
										</th>
									</tr>
									<tr>
										<th>ID</th>
										<th>ROLLNO</th>
										<th>NAME</th>
										<th>ADDRESS</th>
										<th>ACTIONS</th>	
											
									</tr>";
									if (mysqli_num_rows($result)>0) {
										while($row1 = mysqli_fetch_array($result))
										{
											echo "<tr>";
												echo "<td>" . $row1['student_id'] . "</td>";
												echo "<td>" . $row1['student_roll'] . "</td>";
												echo "<td>" . $row1['student_name'] . "</td>";
												echo "<td>" . $row1['student_address'] . "</td>";
												echo "<td><a href='delete.php?id=".$row1['student_id']."'><i class='fa fa-trash'></i></a>
													<a href='update.php?id=".$row1['student_id']."'><i class='fa fa-pencil'></i></a></td>";
												
											echo "</tr>";
										}
									}
									else{
										$error="no data available";
									}
								echo "</table>";
							}
						}
					}
				}
			}
			$student= new Eachclass();
			$student->select_class_info($id);
 		?>
 	</div>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>