<?php 
	include 'session.php';
	include("connection.php");
	$id=$_GET['id'];
	class Dropdown{
		private $connection;
		public function __construct(){
			$connection= new Connection();
			$this->conn=$connection->connect();
		}
		public function select_class_data($id){
			$sql="SELECT * FROM class WHERE classid='$id' ";
			$result=mysqli_query($this->conn,$sql);
			if ($result) {
				echo "<select name='class' class='form-control'>";
					while ($row = mysqli_fetch_array($result)) {
	    				echo "<option value='" . $row['classid'] ."'>Class " . $row['class'] ."</option>";
					}
				echo "</select>";
			}
		}
		public function insert_student_data(){

			$valid=true;
			$class_id="";
			$data = ['name_error' => null, 'roll_error'=> null, 'address_error' => null, 'result' => 0];
			if ($_SERVER['REQUEST_METHOD']=="POST") {
				if (empty($_POST['studentname'])) {
					$valid=false;
					$data['name_error']="username is empty";
				}
				if (empty($_POST['studentroll'])) {
					$valid=false;
					$data['roll_error']="roll no is empty";
				}
				if (empty($_POST['studentaddress'])) {
					$valid=false;
					$data['address_error']="address is empty";
				}
				if ($valid) {
					$student_name=$_POST['studentname'];
					$student_roll=$_POST['studentroll'];
					$student_address=$_POST['studentaddress'];
					$class_id=$_POST['class'];
					$sql="INSERT INTO student(student_roll,student_name,student_address,class_id) VALUES ('$student_roll','$student_name','$student_address',$class_id)";
					$query=mysqli_query($this->conn,$sql);
					if ($query) {
						header('location:classdetail.php');
					}
					$data['result'] = $query;
				}
				return $data;
			}
		}
	}
	$newdata= new Dropdown();
	$query_result = $newdata->insert_student_data();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Students Detail</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style type="text/css">
		.middle{
			margin-top: 5%;
			background:#c1c7c4;
		}
		form{
			margin: 40px;
		}
		form label{
			font-size: 18px;
			font-weight: normal;
		    font-family: monospace;
		    padding-bottom: 2px;
		}
		form input{
			margin-bottom: 17px;
		}
		.class{
			margin-bottom: 2px;
		}
	</style>
</head>
<body>
	<?php include('navbar.php'); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 middle">
				<div class="register-heading">
					<label>Add Student</label>
				</div>
				<form method="POST" action="">
					<div class="form-group">
					<label>RollNo:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentroll">
					 <label>Name:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentname">
					 <label>Address:</label>
					<input type="text" class="form-control" id="exampleInputEmail1" name="studentaddress">
					<label class="class">Class:</label><br>
					<?php 
						$drop= new Dropdown();
						echo $drop->select_class_data($id);
					 ?><br>
					<button type="submit" class="btn btn-primary" name="submit">submit</button>
					</div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>















